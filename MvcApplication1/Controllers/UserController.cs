﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;
using MvcApplication1.DAL;
using WebMatrix.WebData;
using System.Web.Security;

namespace MvcApplication1.Controllers
{
    public class UserController : Controller
    {
        private NeoGraphicsEntities db = new NeoGraphicsEntities();

        //
        // GET: /User/

        public ActionResult Index()
        {
            return View(db.Users.Where(u => u.UserId != 1).ToList());

        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            List<Role> userRoles = db.Roles.Where(r=>r.RoleId!=1).ToList();
            ViewBag.UserRoles = userRoles;
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                WebSecurity.CreateUserAndAccount(user.UserName, "123456", null);

                List<Role> userRoles = db.Roles.ToList();
                string roleName = string.Empty;
                foreach (Role role in userRoles)
                {
                    var result = Request[Convert.ToString(role.RoleId)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            roleName = role.RoleName;
                        }
                    }

                }
                if (!string.IsNullOrEmpty(roleName))
                {
                    if (!Roles.RoleExists(roleName))
                        Roles.CreateRole(roleName);
                    Roles.AddUserToRole(user.UserName, roleName);
                }
                return RedirectToAction("Index");
            }

            return View(user);
        }

        //
        // GET: /User/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}