﻿using MvcApplication1.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class HomeController : Controller
    {
        private NeoGraphicsEntities db = new NeoGraphicsEntities();

        public ActionResult Index()
        {
            ViewBag.Categories = db.Categories.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Facility()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Publish()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Clients()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Faq()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetProducts(int catId)
        {
            var products = db.Products.Where(p => p.CategoryId == catId).ToList();
            return Json(new { Response = products }, JsonRequestBehavior.AllowGet);
        }

    }
}
