﻿using MvcApplication1.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcApplication1.DAL
{
    //database context for Neo Graphics
    public class NeoGraphicsEntities : DbContext
    {
        public NeoGraphicsEntities()
            : base("NeoGraphicsConnection")
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UsersInRole> UsersInRoles { get; set; }

    }
}