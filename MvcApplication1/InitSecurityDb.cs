﻿using MvcApplication1.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;

namespace MvcApplication1
{
    public class InitSecurityDb : DropCreateDatabaseIfModelChanges<NeoGraphicsEntities>
    {
        protected override void Seed(NeoGraphicsEntities context)
        {
            WebSecurity.InitializeDatabaseConnection("NeoGraphicsConnection", "Neo_User", "UserId", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists(Common.UserRoles.SuperAdmin.ToString()))
            {
                roles.CreateRole(Common.UserRoles.SuperAdmin.ToString());
            }

            if (!roles.RoleExists(Common.UserRoles.Admin.ToString()))
            {
                roles.CreateRole(Common.UserRoles.Admin.ToString());
            }

            if (membership.GetUser("SuperAdmin", false) == null)
            {
                membership.CreateUserAndAccount("SuperAdmin", "123456");
            }

            if (!roles.GetRolesForUser("SuperAdmin").Contains("SuperAdmin"))
            {
                roles.AddUsersToRoles(new[] { "SuperAdmin" }, new[] { "SuperAdmin" });
            }
        }
    }
}