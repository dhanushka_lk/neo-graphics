﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Common
{
    public enum Category
    {
        Creative,

        Printing,

        Finishing
    }

    public enum UserRoles
    {
        SuperAdmin,

        Admin,
    }
}